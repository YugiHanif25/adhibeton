# -*- coding: utf-8 -*-
{
    'name': "Stock picking invoice",
    'summary': 'Invoice stock pickings',
    'description': """
        Allow starting Purchase, Sales and refunds from the warehouse,
        And creating invoices on Warehouse pickings.
    """,
    "license": "AGPL-3",
    'author': "Angkringan.com",
    'website': "https://prointegrated.com",
    'category': 'Stock',
    'version': '10.0.0.4',
    'depends': [
        'base','account','stock','product'
    ],
    'data': ['security/ir.model.access.csv','views/templates.xml',],
# 'security/user_groups.xml','security/ir.model.access.csv','views/menu.xml',
    "application": True,
    "installable": True,
    "auto_install": False,
}