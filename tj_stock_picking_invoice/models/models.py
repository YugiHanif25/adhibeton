# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError

class Picking1(models.Model):
    _inherit = 'stock.move'
class Picking2(models.Model):
    _inherit = 'stock.pack.operation'
class Picking3(models.Model):
    _inherit = 'stock.location'
class Picking4(models.Model):
    _inherit = 'stock.picking.type'
class Picking5(models.Model):
    _inherit = 'product.template'
class Picking6(models.Model):
    _inherit = 'res.partner'
class Picking7(models.Model):
    _inherit = 'procurement.group'


class Picking(models.Model):
    _inherit = 'stock.picking'

    invoice_state = fields.Selection(string="Invoice Control", selection=[('none', 'Not Applicable'),
                   ('2binvoiced', 'To Be Invoiced'),
                   ('invoiced', 'Invoiced'),
                   ],  default='2binvoiced',compute='_compute_invoice', store=True, )
    invoice_id = fields.Many2one('account.invoice', string="Account Invoice",readonly=True, store=True, )
    

    @api.one
    @api.depends('picking_type_id')
    def _compute_invoice(self):
        if self.picking_type_id:
            if self.picking_type_id.id in [4,11]:
                self.invoice_state = '2binvoiced'
                self.user_id = self.env.user
                #self.team_id = self.env['crm.team'].search([('name', '=', 'Galon')])
            else:
                self.invoice_state = '2binvoiced'
                self.user_id = False
                self.team_id = False

    

    # Override copy to take into account the invoice_id and status
    # when copying we want to unlink invoice_id and set invoice control to 2binvoiced
    @api.model
    def create(self, vals):
        if vals.get('invoice_id'):
            vals['invoice_id'] = ''
            vals['invoice_state'] = '2binvoiced'
        return super(Picking, self).create(vals)


    @api.multi
    def create_invoice(self):
        invoice_obj = self.env['account.invoice']
        i_line_obj = self.env['account.invoice.line']
        sale_journal = self.env['account.journal'].search([('type','=','sale')])[0]
        sale_journal_id = sale_journal.id
        purch_journal = self.env['account.journal'].search([('type','=','purchase')])[0]
        purch_journal_id = purch_journal.id

        # raise Warning("Sudah Terbentuk Vendor Bill untuk No picking ini...")

        for obj in self:
            if obj.invoice_id :                  
                raise UserError(_("Sudah Terbentuk Vendor Bill untuk No picking ini..."))
            # Production Refund
            if obj.location_dest_id.usage == 'production':
                global inv_id
                inv_id= invoice_obj.create({
                    'partner_id':obj.partner_id.id,
                    'picking_id': self.id,
                    'date_invoice':obj.min_date,
                    'origin':obj.origin,
                    'type':'in_refund',
                    'journal_id':purch_journal_id,
                    'account_id': obj.partner_id.property_account_payable_id.id,                    
                 })
                for i in obj.move_lines:
                    accounts = i.product_id.product_tmpl_id.get_product_accounts()
                    price_unit = i.product_id.uom_id._compute_price(i.product_id.standard_price, i.product_uom)
                    i_line_id = i_line_obj.create({
                    'invoice_id':inv_id.id,
                    'product_id':i.product_id.id,
                    'price_unit': price_unit,
                    'name':i.name,
                    'account_id': accounts.get('stock_input') and accounts['stock_input'].id or \
                                  accounts['expense'].id,
                    'quantity':i.product_uom_qty,
                    'uom_id':i.product_uom.id,
                    })
            # Production Invoice
            elif obj.location_id.usage == 'production':
                    inv_id= invoice_obj.create({
                        'partner_id':obj.partner_id.id,
                        'picking_id': self.id,
                        'date_invoice':obj.min_date,
                        'origin':obj.origin,
                        'type':'in_invoice',
                        'journal_id':purch_journal_id,
                        'account_id': obj.partner_id.property_account_payable_id.id,                        
                            })
                    for i in obj.move_lines:
                        accounts = i.product_id.product_tmpl_id.get_product_accounts()
                        price_unit = i.product_id.uom_id._compute_price(i.product_id.standard_price, i.product_uom)
                        i_line_id = i_line_obj.create({
                        'invoice_id':inv_id.id,
                        'product_id':i.product_id.id,
                        'price_unit': price_unit,
                        'name':i.name,
                        'account_id':accounts.get('stock_input') and accounts['stock_input'].id or accounts['expense'].id,
                        'quantity':i.product_uom_qty,
                        'uom_id':i.product_uom.id,
                        })
            # Vendor Refund
            elif obj.location_dest_id.usage == 'supplier':
                global inv_id
                inv_id= invoice_obj.create({
                    'partner_id':obj.partner_id.id,
                    'picking_id': self.id,
                    'date_invoice':obj.min_date,
                    'origin':obj.origin,
                    'type':'in_refund',
                    'journal_id':purch_journal_id,
                    'account_id': obj.partner_id.property_account_payable_id.id,                    
                 })
                for i in obj.move_lines:
                    accounts = i.product_id.product_tmpl_id.get_product_accounts()
                    price_unit = i.product_id.uom_id._compute_price(i.product_id.standard_price, i.product_uom)
                    i_line_id = i_line_obj.create({
                    'invoice_id':inv_id.id,
                    'product_id':i.product_id.id,
                    'price_unit': i.purchase_line_id.price_unit,
                    'name':i.name,
                    'account_id': accounts.get('stock_input') and accounts['stock_input'].id or \
                                  accounts['expense'].id,
                    'quantity':i.product_uom_qty,
                    'uom_id':i.product_uom.id,
                    })
            # Vendor Invoice
            elif obj.location_id.usage == 'supplier':
                    inv_id= invoice_obj.create({
                        'partner_id':obj.partner_id.id,
                        'picking_id': self.id,
                        'date_invoice':obj.min_date,
                        'origin':obj.origin,
                        'type':'in_invoice',
                        'journal_id':purch_journal_id,
                        'account_id': obj.partner_id.property_account_payable_id.id,                        
                            })
                    for i in obj.move_lines:
                        accounts = i.product_id.product_tmpl_id.get_product_accounts()
                        price_unit = i.product_id.uom_id._compute_price(i.product_id.standard_price, i.product_uom)
                        i_line_id = i_line_obj.create({
                        'invoice_id':inv_id.id,
                        'price_unit': i.purchase_line_id.price_unit,
                        'price_unit': price_unit,
                        'name':i.name,
                        'account_id':accounts.get('stock_input') and accounts['stock_input'].id or accounts['expense'].id,
                        'quantity':i.product_uom_qty,
                        'uom_id':i.product_uom.id,
                        })
            # Customer Refund
            elif obj.location_id.usage == 'customer':
                inv_id= invoice_obj.create({
                    'partner_id':obj.partner_id.id,
                    'picking_id': self.id,
                    'date_invoice':obj.min_date,
                    'origin':obj.origin,
                    'type':'out_refund',
                    'journal_id':sale_journal_id,
                    'account_id': obj.partner_id.property_account_receivable_id.id,
                    # 'user_id': obj.user_id.id,
                    # 'team_id': obj.team_id.id,
                     })
                for i in obj.move_lines:
                    accounts = i.product_id.product_tmpl_id.get_product_accounts()
                    price_unit = i.product_id.uom_id._compute_price(i.product_id.lst_price, i.product_uom)
                    i_line_id = i_line_obj.create({
                        'invoice_id':inv_id.id,
                        'product_id':i.product_id.id,
                        'price_unit':price_unit,
                        'name':i.name,
                        'account_id':accounts.get('income') and accounts['income'].id or False,
                        'quantity':i.product_uom_qty,
                        'uom_id': i.product_uom.id,
                         })

            elif obj.location_dest_id.usage == 'customer':
                inv_id= invoice_obj.create({
                    'partner_id':obj.partner_id.id,
                    'picking_id': self.id,
                    'date_invoice':obj.min_date,
                    'origin':obj.origin,
                    'type':'out_invoice',
                    'journal_id':sale_journal_id,
                    'account_id': obj.partner_id.property_account_receivable_id.id,
                    # 'user_id': obj.user_id.id,
                    # 'team_id': obj.team_id.id,
                     })
                for i in obj.move_lines:
                    accounts = i.product_id.product_tmpl_id.get_product_accounts()
                    price_unit = i.product_id.uom_id._compute_price(i.product_id.lst_price, i.product_uom)
                    i_line_id = i_line_obj.create({
                        'invoice_id':inv_id.id,
                        'product_id':i.product_id.id,
                        'price_unit':price_unit,
                        'name':i.name,
                        'account_id':accounts.get('income') and accounts['income'].id or False,
                        'quantity':i.product_uom_qty,
                        'uom_id': i.product_uom.id,
                        })

            else:
                break

            if inv_id and i_line_id:
                obj.write({'invoice_state':'invoiced'})

            self.invoice_id= inv_id.id        
            return self.action_view_order()

    @api.multi
    def action_view_order(self):
        self.ensure_one()
        if self.invoice_id.type in ('in_invoice','in_refund') :
            action = self.env.ref('account.action_invoice_tree2')
            view_id = self.env.ref('account.invoice_supplier_form')
        elif self.invoice_id.type in ('out_invoice','out_refund') :
            action = self.env.ref('account.action_invoice_tree1')
            view_id = self.env.ref('account.invoice_form')
        else :
            return False
        return {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': action.target,
            'res_model': action.res_model,
            'res_id': self.invoice_id and self.invoice_id.id or 0,
        }

    

class AccountInvoice(models.Model):
    _inherit='account.invoice'
    picking_id = fields.Many2one('stock.picking','Picking invoice',readonly=True, store=True,)

    #ToDo: When deleting set the picking's invoice_id to null and invoice_state to 2binvoiced
    @api.multi
    def unlink(self):
        # res = super(AccountInvoice, self).unlink()
        for invoice in self:
            if invoice.state not in ('draft', 'cancel'):
                raise UserError(_('You cannot delete an invoice which is not draft or cancelled. You should refund it instead.'))
            elif invoice.move_name:
                raise UserError(_('You cannot delete an invoice after it has been validated (and received a number). You can set it back to "Draft" state and modify its content, then re-confirm it.'))
            invoice.picking_id.write({'invoice_state':'2binvoiced'})
        return super(AccountInvoice, self).unlink()
