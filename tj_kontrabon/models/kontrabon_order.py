from odoo import api, fields, models, _
import time
from itertools import groupby
from datetime import datetime, timedelta

from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp

KB_STATES =[
    ('draft','Draft'),
    ('confirm','Confirm'),
    ('done','Done'),
    ('cancel','Cancel'),
]

class KontrabonOrder(models.Model):
    _name = 'kontrabon.order'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'id desc'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('kontrabon.order') or _('New')
            vals['name'] = seq
            result = super(KontrabonOrder, self).create(vals)
            return result

    # @api.model
    # def _get_default_name(self):
    #     return self.env['ir.sequence'].next_by_code('delivery.order')

    # name = fields.Char("Name",)
    # name = fields.Char('DO Reference', size=32,
    #                    # required=True,
    #                    # default=_get_default_name,
    #                    track_visibility='onchange')
    name = fields.Char(string='No Kontrabon', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    tanggal = fields.Date(string="Tanggal", required=False, default=lambda self: time.strftime("%Y-%m-%d"),
                          track_visibility='onchange',)
    state = fields.Selection(string="State", selection=KB_STATES, required=True, readonly=True, default=KB_STATES[0][0], track_visibility='onchange',)

    type_kontra = fields.Selection(string="Type Kontra", selection=[
    ('out_invoice','Piutang'),
    ('in_invoice','Hutang'),
    ], required=True,)

    inv_ids = fields.Many2many('account.invoice', 'kontrabon_order_line', 'kb_id', 'inv_id', "Invoice List")

    partner_id = fields.Many2one('res.partner', string='Partner', change_default=True, index=True , required=True)

    # saleorder_ids = fields.One2many(comodel_name="delivery.order.line", inverse_name="delivery_order_id",
    #                                  string="Saleorder", required=False, track_visibility='onchange',)

    # so_ids = fields.Many2many('sale.order', 'delivery.order.line', 'delivery_order_id.id', 'soline.id', '"SO List")
    # saleorder_ids = fields.One2many(comodel_name="delivery.order.line", inverse_name="so_id",
    #                                  string="Saleorder", required=False, track_visibility='onchange',)

    collector_id = fields.Many2one(comodel_name="hr.employee", string="Collector", required=False, store=True,
                               domain=[('job_id.name', 'in', ('COLLECTOR','Collector','collector','SALES','Sales','sales'))], track_visibility='onchange',)

    @api.multi
    def action_draft(self):
        self.state = KB_STATES[0][0]

    @api.multi
    def action_confirm(self):
        self.state = KB_STATES[1][0]
        self.inv_ids.write({'kontrabon_id':self.id})

    @api.multi
    def action_cancel(self):
        self.state = KB_STATES[3][0]
        self.inv_ids.write({'kontrabon_id': False})

    @api.multi
    def action_done(self):
        self.state = KB_STATES[2][0]

#    @api.one
#    @api.depends('inv_ids')
#    def _compute_qty_total(self):
#        draft_orders = self.env['sale.order.line'].search([('order_id', 'in', (self.so_ids.ids)),('product_uom.name', 'in', ('KARTON','GALON'))])
#        tot_qty = sum(draft_orders.mapped('product_uom_qty'))
#        for record in self:
#            record.tot_qty = tot_qty
#        print "draft_orders=>", draft_orders, self.so_ids.ids
#        print "_compute_qty_total=>", tot_qty

    

# class Inherit_Sale_order(models.Model):
#     _inherit = 'sale.order'

    # soqty = fields.Float('Total Qty',compute="call_compute_qty_total",)
    # doqty = fields.Char('Send Qty')
    # dostate = fields.Char('Dostate')

    # so_ids = fields.Many2many('sale.order', 'delivery_order_line', 'do_id', 'so_id', "Sale Order List",
    #                           domain=[('state', 'not in', ('draft', 'cancel'))], )

    # @api.one
    # def call_compute_qty_total(self):
    #     self.env["delivery.order"]._compute_qty_total

        # @api.model
        # def create(self, search, valdoname, valdoqty, valdostate):
        #     draft_orders = self.env['delivery.rumus.insentif'].search([('id', '=', search))
        #     self.write({'doname': valdoname})
        #     self.write({'doqty': valdoqty})
        #     self.write({'dostate': valdostate})

        # nilai = 0
        # self.rit = '1'
        # if self.rit == '1':
        #     nilai = 1
        # if self.rit == '2':
        #     nilai = 1.5
        # if self.rit == '3':
        #     nilai = 2
        # if self.rit == '4':
        #     nilai = 2.5
        # if self.rit == '5':
        #     nilai = 3

# class DeliveryOrderLine(models.Model):
#     _name = 'delivery.order.line'

    # name = fields.Char("nomor So", related="so_id.name", store=True)
    # so_id = fields.Many2one("sale.order", "SO", required=True)
    # amount_total = fields.Float("Total SO", compute="_compute_amount_total2", store=True)
    # total_quantity = fields.Float("Total Quantity", compute="_hitung_quantity_perso", store=True)

    # @api.one
    # @api.depends("so_id")
    # def _hitung_quanity_perso(self):
    #     for me in self:
    #         total_qty = 0
    #
    #     for line in me.so_id.order_line:
    #         total_qty += line.product_uom_qty  # saya lupa nama field quantity di so line. tggl ganti aja
    #         print "loop total_qty=>", total_qty
    #     self.total_quantity = total_qty
    #     print "_hitung_quanity_perso=>", total_qty

    # name = fields.Char("Name", related="delivery_order_id.name", store=True, )
    # delivery_order_id = fields.Many2one(comodel_name="delivery.order", string="Nomor Do", )
    # soline = fields.Many2one(comodel_name="sale.order", string="SO", required=True, )
    # customer = fields.Char("Customer", related='soline.partner_id.name', store=True)
    # qty_total = fields.Float("Qty", compute="_compute_qty_total", readonly=True, store=True)
    # amount_total = fields.Float("Total", compute="_compute_amount_total", readonly=True, store=True)

    # digits = lambda cr: (32, 32),
    # @api.one
    # def _compute_price_total(self):
    #     self.price_total = sum(self.soline.id.mapped('amount_total'))
    #     # self.price_total = sum(self.mapped('soline').mapped('amoun_total'))
    #     print "_compute_price_total=>", self.price_total

    # @api.onchange('soline')
    # @api.one
    # @api.multi
    # @api.one
    # @api.depends("soline")
    # def _compute_amount_total(self):
    #     draft_orders = self.env['sale.order'].search([('id', '=', self.soline.id)])
    #     self.amount_total = sum(draft_orders.mapped('amount_total'))
    #     # for record in self:
    #     #     record.amount_total = amount_total
    #     # for me in self:
    #     #     total_qty = 0
    #     # for line in me.so_id.order_line:
    #     #     total_qty += line.product_uom_qty  # saya lupa nama field quantity di so line. tggl ganti aja
    #     # self.amount_total = total_qty
    #
    #     print "draft_orders=>", draft_orders, self.soline.id
    #     print "_compute_amount_total=>", self.amount_total
    #
    # # @api.onchange('soline')
    # # @api.one
    # # @api.multi
    # @api.depends('soline')
    # def _compute_qty_total(self):
    #     draft_orders = self.env['sale.order.line'].search([('order_id', '=', self.soline.id)])
    #     self.qty_total = sum(draft_orders.mapped('product_uom_qty'))
    #     # for record in self:
    #     #     record.qty_total = qty_total
    #     print "draft_orders=>", draft_orders, self.soline.id
    #     print "_compute_qty_total=>", self.qty_total


    # comment = fields.Char(compute='_compute_comment')
    #
    # def _compute_comment(self):
    #     for record in self:
    #         record.comment = partner_id.comment
    #         print record.comment
    # saleorderline_ids = fields.One2many(comodel_name="delivery.order", inverse_name="soline",
    #                                  string="deliveryorder", required=False, track_visibility='onchange',)
    # print("total one2may= ",saleorderline_ids)
# class DoRelated(models.Model):
#     _inherit = 'stock.move'
#
#     _column = {'user_id': fields.related('picking_id', 'user_id', relation="res.users", type='many2one', string="user",
#                                           store=True, readonly=True)}
#     print 'user_id'
#     print "user_id"
# class SaleOrder(models.Model):
#     _inherit = 'sale.order'
#
#     # do_id = fields.Char("Name", related="delivery_order_id.name", store=True, )
#     do_id = fields.Char("do_id", related="delivery_order_id.name", store=True, )
#     delivery_order_id = fields.Many2one(comodel_name="delivery.order", string="Nomor Do", )