from odoo import api, fields, models

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    kontrabon_id = fields.Many2one('kontrabon.order', 'Kontrabon', copy=False, track_visibility='onchange',)
