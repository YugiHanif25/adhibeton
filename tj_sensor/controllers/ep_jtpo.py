# -*- coding: utf-8 -*-
import logging
_logger = logging.getLogger(__name__)
import math
from datetime import datetime
try:
    import json
    import requests
except Exception as e:
     _logger.error(e)
     _logger.warning("tes")

import io
from odoo import http, _, exceptions
from odoo.http import request
import os
from odoo.tools.mimetypes import guess_mimetype
import base64


def error_response(error, msg):
    return {
        "jsonrpc": "2.0",
        "id": None,
        "error": {
            "code": 404,
            "message": msg,
            "data": {
                "name": str(error),
                "debug": "",
                "message": msg,
                "arguments": list(error.args),
                "exception_type": type(error).__name__
            }
        }
    }
    
class ApiPO(http.Controller):
    @http.route('/api/v1/po', type='http', auth='none', methods=["GET","OPTIONS"], csrf=False)
    def get_all_po(self,**params):
        try:
            prev_page = None
            next_page = None
            total_page_number = 1
            current_page = 1
            kode_lokasi = ""
            domain_lokasi = ("po_location",'!=',False)
            if "kode_lokasi" in params:
                if params['kode_lokasi'] == "sadang" or params['kode_lokasi'] == 'Sadang':domain_lokasi = ('po_location', '=', 'Sadang')
            po_ids = request.env['purchase.order.outstanding'].sudo().search([domain_lokasi])
            total = len(po_ids)
            if "page_size" in params:
                page_size = int(params['page_size'])
                count = len(po_ids)
                total_page_number = math.ceil(count/page_size)
                if "page" in params:
                    current_page = int(params['page'])
                else:
                    current_page = 1
                start = page_size*(current_page-1)
                stop = current_page*page_size
                po_ids = po_ids[start:stop]
                next_page = current_page+1 \
                    if 0 < current_page + 1 <= total_page_number \
                    else None
                prev_page = current_page-1 \
                    if 0 < current_page - 1 <= total_page_number \
                    else None
            data = []
            if len(po_ids) > 0:
                for po in po_ids:
                    data.append({
                        "id":po.id,
                        "no_po":po.no_po, 
                        "kd_vendor":po.kd_vendor, 
                        "vendor":po.vendor, 
                        "kd_item":po.kd_item, 
                        "nama_item":po.nama_item, 
                        "sisa":po.sisa,
                        "date":po.date                   
                    })    
                response = {
                "success":True,
                "message":"Requests Successfull",
                "meta":{
                    "total_data":total,
                    "length":len(po_ids),
                    "total_pages":total_page_number,
                    "pages":current_page,
                    "previous":prev_page,
                    "next":next_page
                },
                "data":data,
            }
                return http.Response(
                    json.dumps(response,default=str),
                    status=200,
                    mimetype='application/json'
                )
            else:
                response = {
                "success":False,
                "message":"id not found",
                "data":[]
            }
                return http.Response(
                    json.dumps(response,default=str),
                    status=200,
                    mimetype='application/json'
                )
        except KeyError as e:
            response = {
                    "success":False,
                    "message":e,
                    "data":[]
                }
            return http.Response(
                    json.dumps(response,default=str),
                    status=200,
                    mimetype='application/json'
                )
            
    
    
    @http.route('/api/v1/measure', type='http', auth='none', methods=["POST","OPTIONS"], csrf=False)
    def create_timbangan(self,**params):
        try:    
                no_po             = params["no_po"] if "no_po" in params else False         
                no_pol            = params["no_pol"] if "no_pol" in params else False       
                date              = params["date"] if "date" in params else False         
                partner_id        = params["partner_id"] if "partner_id" in params else False  
                product_id        = params["product_id"] if "product_id" in params else False   
                reference         = params["reference"] if "reference" in params else False    
                timbangan1        = params["timbangan1"] if "timbangan1" in params else False    
                timbangan2        = params["timbangan2"] if "timbangan2" in params else False    
                quantity          = params["quantity"] if "quantity" in params else False    
                quantity_konversi = params["quantity_konversi"] if "quantity_konversi" in params else False    
                location_id       = params["location_id"] if "location_id" in params else False    
                image1            = params["image1"] if "image1" in params else False    
                image2            = params["image2"] if "image2" in params else False    
                image3            = params["image3"] if "image3" in params else False    
                image4            = params["image4"] if "image4" in params else False    
                image5            = params["image5"] if "image5" in params else False    
                image6            = params["image6"] if "image6" in params else False    
                _logger.warning(image1)
                timbangan = request.env['timbangan'].sudo().create({
                                "no_po":no_po,                    
                                "no_pol":no_pol,
                                "date":datetime.now(),
                                "partner_id":partner_id,       
                                "product_id":product_id,       
                                "reference":reference,       
                                "timbangan1":timbangan1,       
                                "timbangan2":timbangan2,       
                                "quantity":quantity,       
                                "quantity_konversi":quantity_konversi,       
                                "location_id":location_id,
                                "image1":base64.b64encode(image1.read()),       
                                "image2":base64.b64encode(image2.read()),       
                                "image3":base64.b64encode(image3.read()),       
                                "image4":base64.b64encode(image4.read()),       
                                "image5":base64.b64encode(image5.read()),       
                                "image6":base64.b64encode(image6.read()),       
                                
                            })
                
                if timbangan:
                    response = {
                        "success":True,
                        "message":"Requests Successfull",
                        "data":{"quantity":timbangan.quantity,
                                "quantity_konversi_compute":timbangan.quantity_konversi_compute},
                                }
                    return http.Response(
                        json.dumps(response,default=str),
                        status=200,
                        mimetype='application/json'
                    )
                else:
                    response = {
                    "success":False,
                    "message":"id not found",
                    "data":[]
                }
                    return http.Response(
                        json.dumps(response,default=str),
                        status=200,
                        mimetype='application/json'
                    )
        except Exception as e:
            response = {
                    "success":False,
                    "message":e,
                    "data":[]
                }
            return http.Response(
                    json.dumps(response,default=str),
                    status=200,
                    mimetype='application/json'
                )

