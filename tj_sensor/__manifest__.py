# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2019-TODAY Cybrosys Technologies (<https://www.cybrosys.com>).
#    Authors: Tintuk Tomin (<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################
{
    'name': 'Sensor Batching Plan',
    'version': '12.0.1.0.0',
    'summary': """Manage the Batching Plan documents and their expiry notifications""",
    'description': 'Manages the Batching Plan documents and its expiry notifications as emails to the managers.',
    'category': 'Manufacturing',
    'author': 'WIBICON',
    'company': 'WIBICON',
    'maintainer': 'WIBICON',
    'website': "https://www.wibicon.com",
    'depends': ['hr','base','mrp'],
    'data': [        
            "views/sensor.xml",
            "views/pull_from_sunfish.xml",
            "views/product.xml",
            "wizard/sunfish.xml",
            "data/ir_sequence_data.xml",
            "data/cron.xml",
            # "security/ir.model.access.csv",            
    ],
    # 'images': ['static/description/banner.png'],
    'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
