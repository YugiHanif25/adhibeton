from odoo import fields,api,models


class Product(models.Model):
    _inherit='product.product'

    density = fields.Integer('Density',default=1)
    scale   = fields.Float('Scale',default=1)


class ProductTemplate(models.Model):
    _inherit='product.template'

    density = fields.Integer('Density',related='product_variant_id.density')
    scale   = fields.Float('Scale',related='product_variant_id.scale')