from odoo import models, fields, api, _
from odoo.exceptions import Warning,UserError
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import *
import logging
_logger = logging.getLogger(__name__)

try:
    import requests
    import urllib.request
except Exception as e:
    _logger.error("Package Not Found !! Because"+ str(e))
# from odoo import api, fields, models, _
# import time
# from itertools import groupby
# from datetime import datetime, timedelta

# from odoo.exceptions import UserError
# from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
# from odoo.tools.misc import formatLang



#import odoo.addons.decimal_precision as dp

STATE_COLOR_SELECTION = [
    ('0', 'Red'),
    ('1', 'Green'),
    ('2', 'Blue'),
    ('3', 'Yellow'),
    ('4', 'Magenta'),
    ('5', 'Cyan'),
    ('6', 'Black'),
    ('7', 'White'),
    ('8', 'Orange'),
    ('9', 'SkyBlue')
]

STATES =[
    ('draft','Draft'),
    ('confirm','Confirm'),
    ('done','Done'),
    ('cancel','Cancel'),
]

class BatchOrder(models.Model):
    _name = 'batch.order'
    # _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'id desc'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('batch.order') or _('New')
            vals['name'] = seq
            result = super(BatchOrder, self).create(vals)
            return result


    # @api.model
    # def _get_default_name(self):
    #     return self.env['ir.sequence'].next_by_code('delivery.order')

    # name = fields.Char("Name",)
    # name = fields.Char('DO Reference', size=32,
    #                    # required=True,
    #                    # default=_get_default_name,
    #                    track_visibility='onchange')
    name = fields.Char(string='No Order', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    reference = fields.Char(string='Reference', copy=False, )
    no_jo = fields.Char(string='No JO', copy=False, )
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    tanggal = fields.Datetime(
        'Date Plan',
        default=fields.Datetime.now)
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)
    ticket = fields.Char(string="Ticket") 
    type_batch = fields.Selection(string="Type Batch", selection=[
    ('type1','Type 1'),
    ('type2','Type 2'),
    ], required=True, default="type1")

    batchingplan = fields.Selection(string="Batching Plan", selection=[
    ('bp1','Bactching Plan 1 Sadang'),
    ('bp2','Bactching Plan 2 Sadang'),
    ('bp3','Bactching Plan 3 Seulawah'),
    ('bp4','Bactching Plan 4 Indrapuri'),
    ('bp5','Bactching Plan 5'),
    ('bp6','Bactching Plan 6'),
    ('bp7','Bactching Plan 7'),
    ('bp8','Bactching Plan 8'),
    ('bp9','Bactching Plan 9'),
    ('bp10','Bactching Plan 10'),
    ], required=True, default="bp1")

    # quantity = fields.Float(
    #     'Quantity Kg',
    #     default=0.0)

    quantity_result = fields.Float(string='Quantity Result', default=0.0)

    quantity = fields.Float(compute='_compute_amount_all', string='Quantity Kg', default=0.0, store=True)

    quantity_kubik = fields.Float(
        'Quantity Kubik',
        default=0.0)

    # line_ids = fields.Many2many('batch.orde.line','batch_id', "Batch List")
    batch_ids = fields.One2many('batch.order.line', 'batch_id', "Batch List")

    partner_id = fields.Many2one('res.partner', string='Partner', change_default=True, index=True)

    # saleorder_ids = fields.One2many(comodel_name="delivery.order.line", inverse_name="delivery_order_id",
    #                                  string="Saleorder", required=False, track_visibility='onchange',)

    # so_ids = fields.Many2many('sale.order', 'delivery.order.line', 'delivery_order_id.id', 'soline.id', '"SO List")
    # saleorder_ids = fields.One2many(comodel_name="delivery.order.line", inverse_name="so_id",
    #                                  string="Saleorder", required=False, track_visibility='onchange',)

    employee_id = fields.Many2one(comodel_name="hr.employee", string="Operator", required=False, store=True,
                               domain=[('job_id.name', 'in', ('OPERATOR','Operator','operator','KABAG','Kabag','kabag'))], track_visibility='onchange',)
    notes = fields.Text("Notes")

    @api.multi
    def action_draft(self):
        self.state = STATES[0][0]

    @api.multi
    def action_confirm(self):
        self.state = STATES[1][0]
        # self.inv_ids.write({'kontrabon_id':self.id})

    @api.multi
    def action_cancel(self):
        self.state = STATES[3][0]
        # self.inv_ids.write({'kontrabon_id': False})

    @api.multi
    def action_done(self):
        self.state = STATES[2][0]

    @api.multi
    @api.depends('batch_ids.loadcell_1','batch_ids.loadcell_2','batch_ids.loadcell_3','batch_ids.loadcell_4','quantity')
    def _compute_amount_all(self):
        for order in self:            
            order.quantity = 0.0                                    
            order.quantity = sum(line.loadcell_1 + line.loadcell_2 + line.loadcell_3 + line.loadcell_4 for line in order.batch_ids)
            # order.quantity = currency.round(sum(line.loadcell_1 for line in order.batch_ids))
            # order.amount_total = order.amount_tax + amount_untaxed + order.ongkir - order.discount - order.pembulatan + order.upcharge

    # @api.multi
    # @api.depends('batch_ids')
    # def _compute_qty_total(self):
    #         for me in self:
    #         tot_quantity = 0
 
    #     for line in me.batch_ids:
    #         tot_quantity += line.loadcell_1 + line.loadcell_2 + line.loadcell_3 + line.loadcell_4  # saya lupa nama field quantity di so line. tggl ganti aja
    #         print "loop total_qty=>", tot_quantity
    #     self.quantity = tot_quantity
    #     print "_hitung_quanity_perso=>", tot_quantity

    

# class Inherit_Sale_order(models.Model):
#     _inherit = 'sale.order'

    # soqty = fields.Float('Total Qty',compute="call_compute_qty_total",)
    # doqty = fields.Char('Send Qty')
    # dostate = fields.Char('Dostate')

    # so_ids = fields.Many2many('sale.order', 'delivery_order_line', 'do_id', 'so_id', "Sale Order List",
    #                           domain=[('state', 'not in', ('draft', 'cancel'))], )

    # @api.one
    # def call_compute_qty_total(self):
    #     self.env["delivery.order"]._compute_qty_total

        # @api.model
        # def create(self, search, valdoname, valdoqty, valdostate):
        #     draft_orders = self.env['delivery.rumus.insentif'].search([('id', '=', search))
        #     self.write({'doname': valdoname})
        #     self.write({'doqty': valdoqty})
        #     self.write({'dostate': valdostate})

        # nilai = 0
        # self.rit = '1'
        # if self.rit == '1':
        #     nilai = 1
        # if self.rit == '2':
        #     nilai = 1.5
        # if self.rit == '3':
        #     nilai = 2
        # if self.rit == '4':
        #     nilai = 2.5
        # if self.rit == '5':
        #     nilai = 3

# class DeliveryOrderLine(models.Model):
#     _name = 'delivery.order.line'

    # name = fields.Char("nomor So", related="so_id.name", store=True)
    # so_id = fields.Many2one("sale.order", "SO", required=True)
    # amount_total = fields.Float("Total SO", compute="_compute_amount_total2", store=True)
    # total_quantity = fields.Float("Total Quantity", compute="_hitung_quantity_perso", store=True)

    # @api.one
    # @api.depends("so_id")
    # def _hitung_quanity_perso(self):
    #     for me in self:
    #         total_qty = 0
    #
    #     for line in me.so_id.order_line:
    #         total_qty += line.product_uom_qty  # saya lupa nama field quantity di so line. tggl ganti aja
    #         print "loop total_qty=>", total_qty
    #     self.total_quantity = total_qty
    #     print "_hitung_quanity_perso=>", total_qty

    # name = fields.Char("Name", related="delivery_order_id.name", store=True, )
    # delivery_order_id = fields.Many2one(comodel_name="delivery.order", string="Nomor Do", )
    # soline = fields.Many2one(comodel_name="sale.order", string="SO", required=True, )
    # customer = fields.Char("Customer", related='soline.partner_id.name', store=True)
    # qty_total = fields.Float("Qty", compute="_compute_qty_total", readonly=True, store=True)
    # amount_total = fields.Float("Total", compute="_compute_amount_total", readonly=True, store=True)

    # digits = lambda cr: (32, 32),
    # @api.one
    # def _compute_price_total(self):
    #     self.price_total = sum(self.soline.id.mapped('amount_total'))
    #     # self.price_total = sum(self.mapped('soline').mapped('amoun_total'))
    #     print "_compute_price_total=>", self.price_total

    # @api.onchange('soline')
    # @api.one
    # @api.multi
    # @api.one
    # @api.depends("soline")
    # def _compute_amount_total(self):
    #     draft_orders = self.env['sale.order'].search([('id', '=', self.soline.id)])
    #     self.amount_total = sum(draft_orders.mapped('amount_total'))
    #     # for record in self:
    #     #     record.amount_total = amount_total
    #     # for me in self:
    #     #     total_qty = 0
    #     # for line in me.so_id.order_line:
    #     #     total_qty += line.product_uom_qty  # saya lupa nama field quantity di so line. tggl ganti aja
    #     # self.amount_total = total_qty
    #
    #     print "draft_orders=>", draft_orders, self.soline.id
    #     print "_compute_amount_total=>", self.amount_total
    #
    # # @api.onchange('soline')
    # # @api.one
    # # @api.multi
    # @api.depends('soline')
    # def _compute_qty_total(self):
    #     draft_orders = self.env['sale.order.line'].search([('order_id', '=', self.soline.id)])
    #     self.qty_total = sum(draft_orders.mapped('product_uom_qty'))
    #     # for record in self:
    #     #     record.qty_total = qty_total
    #     print "draft_orders=>", draft_orders, self.soline.id
    #     print "_compute_qty_total=>", self.qty_total


    # comment = fields.Char(compute='_compute_comment')
    #
    # def _compute_comment(self):
    #     for record in self:
    #         record.comment = partner_id.comment
    #         print record.comment
    # saleorderline_ids = fields.One2many(comodel_name="delivery.order", inverse_name="soline",
    #                                  string="deliveryorder", required=False, track_visibility='onchange',)
    # print("total one2may= ",saleorderline_ids)
# class DoRelated(models.Model):
#     _inherit = 'stock.move'
#
#     _column = {'user_id': fields.related('picking_id', 'user_id', relation="res.users", type='many2one', string="user",
#                                           store=True, readonly=True)}
#     print 'user_id'
#     print "user_id"
# class SaleOrder(models.Model):
#     _inherit = 'sale.order'
#
#     # do_id = fields.Char("Name", related="delivery_order_id.name", store=True, )
#     do_id = fields.Char("do_id", related="delivery_order_id.name", store=True, )
#     delivery_order_id = fields.Many2one(comodel_name="delivery.order", string="Nomor Do", )


class BatchOrderLine(models.Model):
    _name = 'batch.order.line'
    # _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'id desc'

    # @api.model
    # def _get_default_name(self):
    #     return self.env['ir.sequence'].next_by_code('delivery.order')

    # name = fields.Char("Name",)
    # name = fields.Char('DO Reference', size=32,
    #                    # required=True,
    #                    # default=_get_default_name,
    #                    track_visibility='onchange')
    name = fields.Char(string='No Order', required=True, copy=False, readonly=True, index=True,)
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    date_plane = fields.Datetime(
        'Mulai Batch',
        default=fields.Datetime.now)
    date_production = fields.Datetime(
        'Selesai Batch',
        default=fields.Datetime.now)

    batchingplan = fields.Selection(string="Batching Plan", selection=[
    ('bp1','Bactching Plan 1 Sadang'),
    ('bp2','Bactching Plan 2 Sadang'),
    ('bp3','Bactching Plan 3 Indrapuri'),
    ('bp4','Bactching Plan 4 Seulawah'),
    ('bp5','Bactching Plan 5'),
    ('bp6','Bactching Plan 6'),
    ('bp7','Bactching Plan 7'),
    ('bp8','Bactching Plan 8'),
    ('bp9','Bactching Plan 9'),
    ('bp10','Bactching Plan 10'),
    ], required=True, default="bp1")

    # date_plane = fields.Date(string="Data Planning", required=False, default=lambda self: time.strftime("%Y-%m-%d"),
    #                       track_visibility='onchange',)
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)    
    # batch_id =fields.Many2one("batch.order", "Order No", ondelete="cascade")
    # batch_id = fields.Many2one('batch.order', "Batch")
    # batch_id =fields.Many2one("batch.order", "Order No", ondelete="set null")
    batch_id =fields.Many2one("batch.order", string="Order No",)
     # ondelete="cascade"
    # mesin_id = fields.Many2one("mrp.machine", "No Mesin")
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False,
                                  track_visibility='onchange', )
    shift = fields.Selection([('non','Non Shift'), ('pagi','Shift Pagi'),('siang','Shift Siang'),('malam','Shift Malam')],
                             string="Shift", track_visibility='onchange', default="pagi" ) #compute="_compute_state"    
    group_op = fields.Selection([('1','Group 1'),('2','Group 2'),('3','Group 3'),('4','Group 4'),('5','Group 5'),('6','Group 6'),('7','Group 7'),('8','Group 8'),('9','Group 9')],
                             string="Group OP", track_visibility='onchange', default="1" ) #compute="_compute_state"    
    
    # date_production = fields.Date(string="Date Production", required=False, default=lambda self: time.strftime("%Y-%m-%d"),
    #                       track_visibility='onchange',)
    loadcell_1 = fields.Float(
        'Pasir',
        default=0.0)
    loadcell_2 = fields.Float(
        'Split',
        default=0.0)
    loadcell_3 = fields.Float(
        'Cement',
        default=0.0)
    loadcell_4 = fields.Float(
        'Water',
        default=0.0)

    counter_1 = fields.Float(
        'ADX 1',
        default=0.0)
    counter_2 = fields.Float(
        'ADX 2',
        default=0.0)
    counter_3 = fields.Float(
        'ADX 3',
        default=0.0)
    counter_4 = fields.Float(
        'ADX 4',
        default=0.0)


    persen_loadcell_1 = fields.Float(compute='_compute_amount_all', string='% Pasir', default=0.0, store=True)
    persen_loadcell_2 = fields.Float(compute='_compute_amount_all', string='% Split', default=0.0, store=True)
    persen_loadcell_3 = fields.Float(compute='_compute_amount_all', string='% Cement', default=0.0, store=True)
    persen_loadcell_4 = fields.Float(compute='_compute_amount_all', string='% Water', default=0.0, store=True)

    persen_counter_1 = fields.Float(compute='_compute_amount_all', string='% ADX 1', default=0.0, store=True)
    persen_counter_2 = fields.Float(compute='_compute_amount_all', string='% ADX 2', default=0.0, store=True)
    persen_counter_3 = fields.Float(compute='_compute_amount_all', string='% ADX 3', default=0.0, store=True)
    persen_counter_4 = fields.Float(compute='_compute_amount_all', string='% ADX 4', default=0.0, store=True)

    mrp_bom_id = fields.Many2one(comodel_name="mrp.bom", string="BOM", required=False,
                                  track_visibility='onchange', )

    image1 = fields.Binary('Image 1',)
    image2 = fields.Binary('Image 2',)

    io_1 = fields.Boolean(string='+ Pasir 1', default=False)
    io_2 = fields.Boolean(string='+ Pasir 2', default=False)
    io_3 = fields.Boolean(string='+ Split 3', default=False)
    io_4 = fields.Boolean(string='+ Split 4', default=False)
    io_5 = fields.Boolean(string='+ Cmnt 1', default=False)
    io_6 = fields.Boolean(string='+ Cmnt 2', default=False)
    io_7 = fields.Boolean(string='+ Cmnt 3', default=False)
    io_8 = fields.Boolean(string='+ Water', default=False)
    io_9 = fields.Boolean(string='Agg Belt Start', default=False)
    io_10 = fields.Boolean(string='IO 10', default=False)
    io_11 = fields.Boolean(string='IO 11', default=False)
    io_12 = fields.Boolean(string='IO 12', default=False)
    io_13 = fields.Boolean(string='IO 13', default=False)
    io_14 = fields.Boolean(string='IO 14', default=False)
    io_15 = fields.Boolean(string='IO 15', default=False)
    io_16 = fields.Boolean(string='IO 16', default=False)
    io_17 = fields.Boolean(string='IO 17', default=False)
    io_18 = fields.Boolean(string='IO 18', default=False)
    io_19 = fields.Boolean(string='IO 19', default=False)
    io_20 = fields.Boolean(string='IO 20', default=False)
    io_21 = fields.Boolean(string='IO 21', default=False)
    io_22 = fields.Boolean(string='IO 22', default=False)
    io_23 = fields.Boolean(string='IO 23', default=False)
    io_24 = fields.Boolean(string='IO 24', default=False)
    io_25 = fields.Boolean(string='IO 25', default=False)
    io_26 = fields.Boolean(string='IO 26', default=False)
    io_27 = fields.Boolean(string='IO 27', default=False)
    io_28 = fields.Boolean(string='IO 28', default=False)
    io_29 = fields.Boolean(string='IO 29', default=False)
    io_30 = fields.Boolean(string='IO 30', default=False)
    io_31 = fields.Boolean(string='IO 31', default=False)
    io_32 = fields.Boolean(string='IO 32', default=False)

    # product_uom = fields.Many2one(
    #     'product.uom', 'Unit of Measure', required=True)
    notes = fields.Text("Notes")

    @api.multi
    def action_draft(self):
        self.state = STATES[0][0]

    @api.multi
    def action_confirm(self):
        self.state = STATES[1][0]
        # self.inv_ids.write({'kontrabon_id':self.id})

    @api.multi
    def action_cancel(self):
        self.state = STATES[3][0]
        # self.inv_ids.write({'kontrabon_id': False})

    @api.multi
    def action_done(self):
        self.state = STATES[2][0]

        # 

    # @api.onchange('loadcell_1','loadcell_2','loadcell_3','loadcell_4','counter_1','counter_2','counter_3','counter_4','persen_loadcell_1','persen_loadcell_2','persen_loadcell_3','persen_loadcell_4','persen_counter_1','persen_counter_2','persen_counter_3','persen_counter_4')    
    # def on_change_persen_bom(self):    
    #         if record.loadcell_1 and record.loadcell_2 and record.loadcell_3 and record.loadcell_4 and record.counter_1 and record.counter_2 and record.counter_3 and record.counter_4:
    #             record.persen_loadcell_1 = (float(record.loadcell_1)/(record.loadcell_1+record.loadcell_2+record.loadcell_3+record.loadcell_4))*100
    #             record.persen_loadcell_2 = ((float(record.loadcell_2))/(record.loadcell_1+record.loadcell_2+record.loadcell_3+record.loadcell_4))*100
    #             record.persen_loadcell_3 = ((float(record.loadcell_3))/(record.loadcell_1+record.loadcell_2+record.loadcell_3+record.loadcell_4))*100
    #             record.persen_loadcell_4 = ((float(record.loadcell_4))/(record.loadcell_1+record.loadcell_2+record.loadcell_3+record.loadcell_4))*100


    @api.multi
    @api.depends('loadcell_1','loadcell_2','loadcell_3','loadcell_4','counter_1','counter_2','counter_3','counter_4','persen_loadcell_1','persen_loadcell_2','persen_loadcell_3','persen_loadcell_4','persen_counter_1','persen_counter_2','persen_counter_3','persen_counter_4')
    def _compute_amount_all(self):        
        for order in self:            
            tot_pembagi = (order.loadcell_1+order.loadcell_2+order.loadcell_3+order.loadcell_4+order.counter_1+order.counter_2+order.counter_3+order.counter_4)
            if tot_pembagi > 0 :
                order.persen_loadcell_1 = 0.0
                order.persen_loadcell_2 = 0.0
                order.persen_loadcell_3 = 0.0
                order.persen_loadcell_4 = 0.0
                order.persen_counter_1 = 0.0
                order.persen_counter_2= 0.0
                order.persen_counter_3 = 0.0
                order.persen_counter_4 = 0.0
                order.persen_loadcell_1 = (float(order.loadcell_1)/(tot_pembagi))*100
                order.persen_loadcell_2 = (float(order.loadcell_2)/(tot_pembagi))*100
                order.persen_loadcell_3 = (float(order.loadcell_3)/(tot_pembagi))*100
                order.persen_loadcell_4 = (float(order.loadcell_4)/(tot_pembagi))*100
                order.persen_counter1 = (float(order.counter_1)/(tot_pembagi))*100
                order.persen_counter2 = (float(order.counter_2)/(tot_pembagi))*100
                order.persen_counter3 = (float(order.counter_3)/(tot_pembagi))*100
                order.persen_counter4 = (float(order.counter_4)/(tot_pembagi))*100

            # order.quantity = sum(line.loadcell_1 + line.loadcell_2 + line.loadcell_3 + line.loadcell_4 for line in order.batch_ids)

    # order.quantity = sum(line.loadcell_1 + line.loadcell_2 + line.loadcell_3 + line.loadcell_4 for line in order.batch_ids)

    # @api.multi
    # @api.depends('batch_ids.loadcell_1','batch_ids.loadcell_2','batch_ids.loadcell_3','batch_ids.loadcell_4','quantity')
    # def _compute_amount_all(self):
    #     for order in self:            
    #         order.quantity = 0.0                                    
    #         order.quantity = sum(line.loadcell_1 + line.loadcell_2 + line.loadcell_3 + line.loadcell_4 for line in order.batch_ids)



    

class BatchOrderLineLog(models.Model):
    _name = 'batch.order.line.log'
    # _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'id desc'

    # @api.model
    # def _get_default_name(self):
    #     return self.env['ir.sequence'].next_by_code('delivery.order')

    # name = fields.Char("Name",)
    # name = fields.Char('DO Reference', size=32,
    #                    # required=True,
    #                    # default=_get_default_name,
    #                    track_visibility='onchange')
    name = fields.Char(string='No Order', required=True, copy=False, readonly=True, index=True,)
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    # date_plane = fields.Date(string="Data Planning", required=False, default=lambda self: time.strftime("%Y-%m-%d"),
    #                       track_visibility='onchange',)
    date_plane = fields.Datetime(
        'Date Plan',
        default=fields.Datetime.now)
    date_production = fields.Datetime(
        'Date Production',
        default=fields.Datetime.now)
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)    

    batchingplan = fields.Selection(string="Batching Plan", selection=[
    ('bp1','Bactching Plan 1 Sadang'),
    ('bp2','Bactching Plan 2 Sadang'),
    ('bp3','Bactching Plan 3 Indrapuri'),
    ('bp4','Bactching Plan 4 Seulawah'),
    ('bp5','Bactching Plan 5'),
    ('bp6','Bactching Plan 6'),
    ('bp7','Bactching Plan 7'),
    ('bp8','Bactching Plan 8'),
    ('bp9','Bactching Plan 9'),
    ('bp10','Bactching Plan 10'),
    ], required=True, default="bp1")
    
    # batch_id = fields.Many2one('batch.order', "Batch")
    # mesin_id = fields.Many2one("mrp.machine", "No Mesin")
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False,
                                  track_visibility='onchange', )
    shift = fields.Selection([('non','Non Shift'), ('pagi','Shift Pagi'),('siang','Shift Siang'),('malam','Shift Malam')],
                             string="Shift", track_visibility='onchange', default="pagi" ) #compute="_compute_state"    
    group_op = fields.Selection([('1','Group 1'),('2','Group 2'),('3','Group 3'),('4','Group 4'),('5','Group 5'),('6','Group 6'),('7','Group 7'),('8','Group 8'),('9','Group 9')],
                             string="Group OP", track_visibility='onchange', default="1" ) #compute="_compute_state"    
    # date_production = fields.Date(string="Date Production", required=False, default=lambda self: time.strftime("%Y-%m-%d"),
    #                       track_visibility='onchange',)    
    loadcell_1 = fields.Float(
        'Pasir',
        default=0.0)
    loadcell_2 = fields.Float(
        'Split',
        default=0.0)
    loadcell_3 = fields.Float(
        'Cement',
        default=0.0)
    loadcell_4 = fields.Float(
        'Water',
        default=0.0)

    counter_1 = fields.Float(
        'ADX 1',
        default=0.0)
    counter_2 = fields.Float(
        'ADX 2',
        default=0.0)
    counter_3 = fields.Float(
        'Counter 3',
        default=0.0)
    counter_4 = fields.Float(
        'Counter 4',
        default=0.0)

    io_1 = fields.Boolean(string='+ Pasir 1', default=False)
    io_2 = fields.Boolean(string='+ Pasir 2', default=False)
    io_3 = fields.Boolean(string='+ Split 3', default=False)
    io_4 = fields.Boolean(string='+ Split 4', default=False)
    io_5 = fields.Boolean(string='+ Cmnt 1', default=False)
    io_6 = fields.Boolean(string='+ Cmnt 2', default=False)
    io_7 = fields.Boolean(string='+ Cmnt 3', default=False)
    io_8 = fields.Boolean(string='+ Water', default=False)
    io_9 = fields.Boolean(string='Agg Belt Start', default=False)
    io_10 = fields.Boolean(string='IO 10', default=False)
    io_11 = fields.Boolean(string='IO 11', default=False)
    io_12 = fields.Boolean(string='IO 12', default=False)
    io_13 = fields.Boolean(string='IO 13', default=False)
    io_14 = fields.Boolean(string='IO 14', default=False)
    io_15 = fields.Boolean(string='IO 15', default=False)
    io_16 = fields.Boolean(string='IO 16', default=False)
    io_17 = fields.Boolean(string='IO 17', default=False)
    io_18 = fields.Boolean(string='IO 18', default=False)
    io_19 = fields.Boolean(string='IO 19', default=False)
    io_20 = fields.Boolean(string='IO 20', default=False)
    io_21 = fields.Boolean(string='IO 21', default=False)
    io_22 = fields.Boolean(string='IO 22', default=False)
    io_23 = fields.Boolean(string='IO 23', default=False)
    io_24 = fields.Boolean(string='IO 24', default=False)
    io_25 = fields.Boolean(string='IO 25', default=False)
    io_26 = fields.Boolean(string='IO 26', default=False)
    io_27 = fields.Boolean(string='IO 27', default=False)
    io_28 = fields.Boolean(string='IO 28', default=False)
    io_29 = fields.Boolean(string='IO 29', default=False)
    io_30 = fields.Boolean(string='IO 30', default=False)
    io_31 = fields.Boolean(string='IO 31', default=False)
    io_32 = fields.Boolean(string='IO 32', default=False)

    # product_uom = fields.Many2one(
    #     'product.uom', 'Unit of Measure', required=True)
    notes = fields.Text("Notes")

    @api.multi
    def action_draft(self):
        self.state = STATES[0][0]

    @api.multi
    def action_confirm(self):
        self.state = STATES[1][0]
        # self.inv_ids.write({'kontrabon_id':self.id})

    @api.multi
    def action_cancel(self):
        self.state = STATES[3][0]
        # self.inv_ids.write({'kontrabon_id': False})

    @api.multi
    def action_done(self):
        self.state = STATES[2][0]


class Timbangan(models.Model):
    _name = 'timbangan'
    _order = 'id desc'


    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('timbangan') or _('New')
            vals['name'] = seq
            result = super(Timbangan, self).create(vals)
            return result

    name = fields.Char(string='No Trans', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    reference = fields.Char(string='No Ref',)
    location_id = fields.Many2one('stock.location', string='Location')
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    date = fields.Datetime(
        'Date',
        default=fields.Datetime.now())
    no_po = fields.Char(string='No PO', )
    partner_id = fields.Many2one('res.partner', string='Vendor', change_default=True, index=True) 
     # , required=True
    no_pol = fields.Char(string='No Polisi', )
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False,
                                  track_visibility='onchange', )
    shift = fields.Selection([('non','Non Shift'), ('pagi','Shift Pagi'),('siang','Shift Siang'),('malam','Shift Malam')],
                             string="Shift", track_visibility='onchange', default="pagi" ) #compute="_compute_state"    
    product_id = fields.Many2one(comodel_name="product.product", string="Product", required=False,
                                  track_visibility='onchange', )
    

    timbangan1 = fields.Float(
        'Timbang 1',
        default=0.0)
    timbangan2 = fields.Float(
        'Timbang 2',
        default=0.0)
    quantity = fields.Float(
        'Quantity',
        default=0.0)
    
    quantity_konversi = fields.Float(
        'Qty Konversi',
        default=0.0)    
    quantity_konversi_compute = fields.Float(compute='_compute_konversi', string='Quantity Komversi Compute', default=0.0, store=True)    

    image1 = fields.Binary('Image 1')
    image2 = fields.Binary('Image 2')
    image3 = fields.Binary('Image 3')
    image4 = fields.Binary('Image 4')
    image5 = fields.Binary('Image 5')
    image6 = fields.Binary('Image 6')
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)    
    notes = fields.Text("Notes")
    qty_manual = fields.Float(
        'Quantity Manual',
        default=0.0)
    
    sunfish_status = fields.Boolean(string='Sunfish Status',default=False)
    
    _sql_constraints = [
        ('po_uniq', 'unique(no_po,reference)', 'Nomor Po Dengan Reference Sudah Ada')
    ]
    
    @api.multi
    def open_wizard_post_to_sunfish(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'post.sunfish.wizard',
            'target': 'new',
        }
    
    @api.one
    @api.depends('quantity','no_po')
    def _compute_konversi(self): 
        if self.no_po:
            out_po = self.env['purchase.order.outstanding'].search([('no_po','=',self.no_po),('vendor','=',self.partner_id.name),('kd_item','=',self.product_id.default_code),('unit_type','=','M3')],limit=1)
            for po in out_po:
                    self.quantity_konversi_compute = (self.quantity * self.product_id.density)/self.product_id.scale
        # else:
        #     self.quantity_konversi_compute = self.quantity

            # for order in self:            
            #     order.quantity_konversi_compute = 0.0                                    
            #     order.quantity_konversi_compute = order.quantity
            #     #order.quantity = sum(line.loadcell_1 + line.loadcell_2 + line.loadcell_3 + line.loadcell_4 for line in order.batch_ids)
            

class TimbanganLog(models.Model):
    _name = 'timbangan.log'
    _order = 'id desc'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('timbangan.log') or _('New')
            vals['name'] = seq
            result = super(TimbanganLog, self).create(vals)
            return result

    name = fields.Char(string='No Trans', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    reference = fields.Char(string='No Ref',)
    location_id = fields.Many2one('stock.location', string='Location')
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    date = fields.Datetime(
        'Date',
        default=fields.Datetime.now)
    no_po = fields.Char(string='No PO', )
    partner_id = fields.Many2one('res.partner', string='Vendor', change_default=True, index=True)
     # , required=True
    no_pol = fields.Char(string='No Polisi', )
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False,
                                  track_visibility='onchange', )
    shift = fields.Selection([('non','Non Shift'), ('pagi','Shift Pagi'),('siang','Shift Siang'),('malam','Shift Malam')],
                             string="Shift", track_visibility='onchange', default="pagi" ) #compute="_compute_state"    
    product_id = fields.Many2one(comodel_name="product.product", string="Product", required=False,
                                  track_visibility='onchange', )
    timbangan = fields.Float(
        'Timbang',
        default=0.0)
    quantity = fields.Float(
        'Quantity',
        default=0.0)
    quantity_konversi = fields.Float(
        'Qty Konversi',
        default=0.0)
    image1 = fields.Binary('Image 1')
    image2 = fields.Binary('Image 2')
    image3 = fields.Binary('Image 3')
    image4 = fields.Binary('Image 4')
    image5 = fields.Binary('Image 5')
    image6 = fields.Binary('Image 6')
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)    
    notes = fields.Text("Notes")
    


class AssetMonitoring(models.Model):
    _name = 'asset.monitoring'
    _order = 'id desc'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('asset.monitoring') or _('New')
            vals['name'] = seq            
            result = super(AssetMonitoring, self).create(vals)               
            return result    
            self.write({'asset_id.sensor1': 12})


    # @api.multi
    # @api.depends('asset_id.sensor1')
    # def _compute_sensor_all(self):        
    #     for order in self:
    #         asset_id.sensor1 = 13
            
            # tot_pembagi = (order.loadcell_1+order.loadcell_2+order.loadcell_3+order.loadcell_4+order.counter_1+order.counter_2+order.counter_3+order.counter_4)
            # if tot_pembagi > 0 :
            #     order.persen_loadcell_1 = 0.0

    name = fields.Char(string='No Trans', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    reference = fields.Char(string='No Ref',)
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    date = fields.Datetime(
        'Date',
        default=fields.Datetime.now)
    asset_no = fields.Char(string='No Asset', default='01' )
    # asset_id = fields.Many2one('asset.asset','asset_id','Asset Name')
    asset_id = fields.Many2one('asset.asset', 'Asset', ondelete="set null", default=1)
    sensor1 = fields.Float(
        'Sensor 1',
        default=0.0)
    sensor2 = fields.Float(
        'Sensor 2',
        default=0.0)
    sensor3 = fields.Float(
        'Sensor 3',
        default=0.0)
    sensor4 = fields.Float(
        'Sensor 4',
        default=0.0)


class AssetMonitoringLog(models.Model):
    _name = 'asset.monitoring.log'
    _order = 'id desc'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('timbangan') or _('New')
            vals['name'] = seq
            result = super(AssetMonitoringLog, self).create(vals)
            return result

    name = fields.Char(string='No Trans', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    reference = fields.Char(string='No Ref',)
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)
    date = fields.Datetime(
        'Date',
        default=fields.Datetime.now)
    asset_no = fields.Char(string='No Asset', default='01' )    
    sensor1 = fields.Float(
        'Sensor 1',
        default=0.0)
    sensor2 = fields.Float(
        'Sensor 2',
        default=0.0)
    sensor3 = fields.Float(
        'Sensor 3',
        default=0.0)
    sensor4 = fields.Float(
        'Sensor 4',
        default=0.0)


class asset_state(models.Model):
    """ 
    Model for asset states.
    """
    _name = 'asset.state'
    _description = 'State of Asset'
    _order = "sequence"

    STATE_SCOPE_TEAM = [
        ('0', 'Finance'),
        ('1', 'Warehouse'),
        ('2', 'Manufacture'),
        ('3', 'Maintenance'),
        ('4', 'Accounting')
    ]

    name = fields.Char('State', size=64, required=True, translate=True)
    sequence = fields.Integer('Sequence', help="Used to order states.", default=1)
    state_color = fields.Selection(STATE_COLOR_SELECTION, 'State Color')
    team = fields.Selection(STATE_SCOPE_TEAM, 'Scope Team')

    def change_color(self):
        color = int(self.state_color) + 1
        if (color>9): color = 0
        return self.write({'state_color': str(color)})


class asset_category(models.Model):
    _description = 'Asset Tags'
    _name = 'asset.category'

    name = fields.Char('Tag', required=True, translate=True)
    asset_ids = fields.Many2many('asset.asset', id1='category_id', id2='asset_id', string='Assets')


class asset_asset(models.Model):
    """
    Assets
    """
    _name = 'asset.asset'
    _description = 'Asset'
    _inherit = ['mail.thread']


    # def _default_sensor1(self):
    #     return self.env['asset.monitoring'].search([('asset_no', '=', '01')], limit=1) #, ('user_id', '=', self.env.uid)
    #         sensor1 = 14

    def _read_group_state_ids(self, domain, read_group_order=None, access_rights_uid=None, team='3'):
        access_rights_uid = access_rights_uid or self.uid
        stage_obj = self.env['asset.state']
        order = stage_obj._order
        # lame hack to allow reverting search, should just work in the trivial case
        if read_group_order == 'stage_id desc':
            order = "%s desc" % order
        # write the domain
        # - ('id', 'in', 'ids'): add columns that should be present
        # - OR ('team','=',team): add default columns that belongs team
        search_domain = []
        search_domain += ['|', ('team','=',team)]
        search_domain += [('id', 'in', ids)]
        stage_ids = stage_obj._search(search_domain, order=order, access_rights_uid=access_rights_uid)
        result = stage_obj.name_get(access_rights_uid, stage_ids)
        # restore order of the search
        result.sort(lambda x,y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))
        return result, {}    

    def _read_group_finance_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '0')

    def _read_group_warehouse_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '1')

    def _read_group_manufacture_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '2')

    def _read_group_maintenance_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '3')
        
    def _read_group_accounting_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '4')

    CRITICALITY_SELECTION = [
        ('0', 'General'),
        ('1', 'Important'),
        ('2', 'Very important'),
        ('3', 'Critical')
    ]

    name = fields.Char('Asset Name', size=64, required=True, translate=True)
    date = fields.Datetime(
        'Date',
        default=fields.Datetime.now)
    finance_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','0')])
    warehouse_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','1')])
    manufacture_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','2')])
    maintenance_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','3')])
    accounting_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','4')])
    maintenance_state_color = fields.Selection(related='maintenance_state_id.state_color', selection=STATE_COLOR_SELECTION, string="Color", readonly=True)
    criticality = fields.Selection(CRITICALITY_SELECTION, 'Criticality')
    property_stock_asset = fields.Many2one(
        'stock.location', "Asset Location",
        company_dependent=True, domain=[('usage', 'like', 'asset')],
        help="This location will be used as the destination location for installed parts during asset life.")
    user_id = fields.Many2one('res.users', 'Assigned to', track_visibility='onchange')
    active = fields.Boolean('Active', default=True)
    asset_number = fields.Char('Asset Number', size=64)
    model = fields.Char('Model', size=64)
    serial = fields.Char('Serial no.', size=64)
    vendor_id = fields.Many2one('res.partner', 'Vendor')
    manufacturer_id = fields.Many2one('res.partner', 'Manufacturer')
    start_date = fields.Date('Start Date')
    purchase_date = fields.Date('Purchase Date')
    warranty_start_date = fields.Date('Warranty Start')
    warranty_end_date = fields.Date('Warranty End')
    image = fields.Binary("Image")
    image_small = fields.Binary("Small-sized image")
    image_medium = fields.Binary("Medium-sized image")
    category_ids = fields.Many2many('asset.category', id1='asset_id', id2='category_id', string='Tags')

    sensor1 = fields.Float(
        'Sensor 1',
        default=0.0)
    sensor2 = fields.Float(
        'Sensor 2',
        default=0.0)
    sensor3 = fields.Float(
        'Sensor 3',
        default=0.0)
    sensor4 = fields.Float(
        'Sensor 4',
        default=0.0)

    date_sensor1 = fields.Datetime(
        'Date Sensor 1')
    date_sensor2 = fields.Datetime(
        'Date Sensor 2')
    date_sensor3 = fields.Datetime(
        'Date Sensor 3')
    date_sensor4 = fields.Datetime(
        'Date Sensor 4')

    _group_by_full = {
        'finance_state_id': _read_group_finance_state_ids,
        'warehouse_state_id': _read_group_warehouse_state_ids,
        'manufacture_state_id': _read_group_manufacture_state_ids,
        'maintenance_state_id': _read_group_maintenance_state_ids,
        'accounting_state_id': _read_group_accounting_state_ids,
    }

    # @api.model
    # def create(self, vals):
    #     tools.image_resize_images(vals)
    #     return super(asset_asset, self).create(vals)

    # @api.multi
    # def write(self, vals):
    #     tools.image_resize_images(vals)
    #     return super(asset_asset, self).write(vals)




class PurchaseOrderOutstanding(models.Model):
    _name = 'purchase.order.outstanding'
    _order = 'id desc'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('purchase.order.outstanding') or _('New')
            vals['name'] = seq            
            result = super(PurchaseOrderOutstanding, self).create(vals)               
            return result    
            

    name = fields.Char(string='No Trans', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    date = fields.Date('Date PO')    
    po_id = fields.Many2one('purchase.order', string='Purchase Order', index=True)
    partner_id = fields.Many2one('res.partner', string='Partner Vendor', change_default=True, index=True)
    product_id = fields.Many2one(comodel_name="product.product", string="Product Item", required=False,
                                  track_visibility='onchange', )
    no_po = fields.Char(string='No PO',)
    po_location = fields.Char('PO Location')
    unit_type   = fields.Char('Unit Type')
    kd_vendor = fields.Char(string='Kode Vendor',)
    vendor = fields.Char(string='Nama Vendor',)
    kd_item = fields.Char(string='Kode Item',)
    nama_item = fields.Char(string='Nama Item',)    
    sisa = fields.Float(
        'Sisa',
        default=0.0)
    description = fields.Text(string="Description", required=False, track_visibility='onchange',)

    #location_ids = fields.Many2many('stock.location','location_id', "Lokasi")
    location_ids = fields.Many2many('stock.location', id1='po_id', id2='location_id', string='Locations', index=True)
    purchase_order_outstanding_ids = fields.One2many('purchase.order.outstanding.line','order_id', string="Purchase Order Outstanding Line", store=True,)
    # location_ids = fields.Many2many('stock.location','purchase_order_outstanding_stock_location_rel','po_id','location_id', string='Locations')
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)


 #    In author-

    # book_ids = fields.Many2many('op.book', 'book_author_rel', 'op_author_id', 'op_book_id', string='Books'),

    # In book-

    # author_ids = fields.Many2many('op.author', 'book_author_rel', 'op_book_id', 'op_author_id', string='Author'),

    #@ock.location

    # kp001 = fields.Boolean('Kantor Pusat', default=False)
    # sdg01 = fields.Boolean('JT Sadang', default=False)
    # sdg02 = fields.Boolean('JT Sadang 2', default=False)
    # ach01 = fields.Boolean('JT Aceh 01', default=False)
    # ach02 = fields.Boolean('JT Aceh 02', default=False)
    # ach03 = fields.Boolean('JT Aceh 03', default=False)
    # ach04 = fields.Boolean('JT Aceh 04', default=False)
    # ach05 = fields.Boolean('JT Aceh 05', default=False)
    # ach06 = fields.Boolean('JT Aceh 06', default=False)
    # ach07 = fields.Boolean('JT Aceh 07', default=False)
    # ach08 = fields.Boolean('JT Aceh 08', default=False)
    # ach09 = fields.Boolean('JT Aceh 09', default=False)
    # ach10 = fields.Boolean('JT Aceh 10', default=False)
    # mjk01 = fields.Boolean('JT Mojokerto', default=False)
    # mjk02 = fields.Boolean('JT Mojokerto 2', default=False)
    # rlb01 = fields.Boolean('JT Risha Lombok', default=False)
    # rlb02 = fields.Boolean('JT Risha Lombok 2', default=False)
    # kgd01 = fields.Boolean('JT Kelapa Gading', default=False)
    # kgd02 = fields.Boolean('JT Kelapa Gading 2', default=False)
    # pcr01 = fields.Boolean('JT Pancoran 1', default=False)
    # pcr02 = fields.Boolean('JT Pancoran 2', default=False)
    # stl01 = fields.Boolean('JT Sentul', default=False)
    # stl02 = fields.Boolean('JT Sentul 2', default=False)
    # kso01 = fields.Boolean('JT KSO APB SIS', default=False)
    # kso02 = fields.Boolean('JT KSO APB SIS 2', default=False)




class PurchaseOrderOutstandingStockLocationRel(models.Model):
    _name = 'purchase.order.outstanding.stock.location.rel'
    _order = 'id desc'

    # @api.model
    # def create(self, vals):
    #     if vals.get('name', _('New')) == _('New'):
    #         seq = self.env['ir.sequence'].next_by_code('purchase.order.outstanding') or _('New')
    #         vals['name'] = seq            
    #         result = super(PurchaseOrderOutstanding, self).create(vals)               
    #         return result    
            
    location_id = fields.Many2one('stock.location', string='Location')
    po_id = fields.Many2one('purchase.order.outstanding', string='Purchase Order')
    # name = fields.Char(string='No Trans', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))

class PurchaseOrderOutstandingLine(models.Model):
    _name = 'purchase.order.outstanding.line'

    order_id = fields.Many2one('purchase.order.outstanding',string="Purchase Order")
    no_kendaraan = fields.Char(string="No Kendaraan")
    tanggal_kendaraan = fields.Date(string="Tanggal Kendaraan")
    partner_id = fields.Many2one('res.partner', related="order_id.partner_id", string='Partner Vendor', change_default=True, index=True)
    product_id = fields.Many2one(comodel_name="product.product", related="order_id.product_id", string="Product Item", required=False,
                                  track_visibility='onchange', )
    kode_item = fields.Char(string="Kode Item", related="order_id.kd_item", readonly=True)
    # item = fields.Many2one('product.product', string="Item", related="order_id.product_id", readonly=True)
    img_1 = fields.Binary(string='Image 1')
    img_2 = fields.Binary(string='Image 2')
    img_3 = fields.Binary(string='Image 3')
    img_4 = fields.Binary(string='Image 4')
    img_5 = fields.Binary(string='Image 5')
    img_6 = fields.Binary(string='Image 6')
    quantity = fields.Float(string='Quantity')
    reference = fields.Char(string='No Surat Jalan')
    state = fields.Selection(string="State", selection=STATES, required=True, readonly=True, default=STATES[0][0], track_visibility='onchange',)

