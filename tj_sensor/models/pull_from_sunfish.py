from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)
try:
    import json
    import urllib.request
    import socket
    from urllib.error import HTTPError, URLError
except Exception as e:
    _logger.warning(e)

class Sunfish(models.Model):
    _name= 'sunfish.pull.logging'	
    
    sunfish_ip = "192.168.100.17"
    # Endpoint Lama
    #   sunfish_url = "http://" +  sunfish_ip + ":80/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=getDataJT"
    sunfish_url                 = "http://" +sunfish_ip + ":80/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=getDataJT2"
    sunfish_density             = "http://" +sunfish_ip+":80/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=getDataDensity"
    sunfish_data_item           = "http://"+sunfish_ip+":80/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=getDataItem"
    sunfish_data_vendor         = "http://"+sunfish_ip+":80/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=getDataVendor"
    number_of_updates           = fields.Integer('Number of updates PO', help='The number of times the scheduler has run and updated this field')
    number_of_updates_product   = fields.Integer('Number of updates Product', help='The number of times the scheduler has run and updated this field')
    number_of_updates_vendor    = fields.Integer('Number of updates Vendor', help='The number of times the scheduler has run and updated this field')
    product_ids                 = fields.Many2many(comodel_name='product.template', string='New Product',readonly=True)
    partner_ids                 = fields.Many2many(comodel_name='res.partner', string='New Product',readonly=True,domain=[('supplier', '=', True)])
    last_modified               = fields.Datetime(string='Last updated', default=fields.Datetime.now())


    def get_data(self):
        l_po = []
        try:
            new_item_product = self.get_data_item()
            new_item_vendor  = self.get_data_vendor()
            po_outstanding = self.env['purchase.order.outstanding'].search([])
            if po_outstanding:
                query = """     
                            DELETE FROM purchase_order_outstanding 
                        """
                self._cr.execute(query)
                row_po  = self._cr.rowcount
                _logger.warning(row_po)
            
            resp = urllib.request.urlopen(self.sunfish_url, timeout=3).read()
            data_json = json.loads(resp.decode('utf-8'))
            if 'datajt' in data_json.keys():
                l_po = data_json['datajt']
                
        
            for po_sunfish in l_po:
                name = self.env['ir.sequence'].next_by_code('purchase.order.outstanding')
                query = """
                            INSERT INTO purchase_order_outstanding 
                                (name,date,no_po,kd_vendor,vendor,kd_item,nama_item,sisa,po_location,unit_type,state) 
                            VALUES
                                ('%s','%s','%s','%s','%s','%s','%s',%s,'%s','%s','%s')
                        """%(name,po_sunfish['PO_Date'],po_sunfish['PO_Number'],po_sunfish['Vend_Code'],po_sunfish['Vend_Name'],po_sunfish['Item_Code'],po_sunfish['Item_Name'],po_sunfish['Remaining_Qty'],po_sunfish['PO_Location'],po_sunfish['Unit_Type'],"draft")
                self._cr.execute(query)
                results  = self._cr.rowcount
                _logger.info(results)
            self.sudo().create({"number_of_updates":len(l_po),"number_of_updates_product":new_item_product,"number_of_updates_vendor":new_item_vendor})
                    
                

        except HTTPError as error:
            _logger.error('Data not retrieved because %s' % error)
            return False



    def get_density(self):
        l_density = []
        try:
            resp = urllib.request.urlopen(self.sunfish_density, timeout=3).read()
            data_json = json.loads(resp.decode('utf-8'))
            if 'dataitem' in data_json.keys():
                l_density = data_json['dataitem']
            return l_density
        except HTTPError as error:
            _logger.error('Data not retrieved because %s' % error)
            return False

    def get_data_item(self):
        l_data_item = []
        try:
            resp = urllib.request.urlopen(self.sunfish_data_item, timeout=3).read()
            data_json = json.loads(resp.decode('utf-8'))
            if 'dataitem' in data_json.keys():
                l_data_item = data_json['dataitem']
            new_item = 0
            for item in l_data_item:
                product = self.env['product.template'].search([('default_code','=',item['Item_Code'])],limit=1)
                if not product:
                    new_product = product.create({
                                    "name":item['Item_Name'],
                                    "default_code":item['Item_Code'],
                                    'uom_id':3,
                                    'categ_id':1
                                })
                    new_item +=1
                    
            return new_item
        except HTTPError as error:
            _logger.error('Data not retrieved because %s' % error)
            return 0
        
    
    def get_data_vendor(self):
        try:
            l_data_item = []
            resp = urllib.request.urlopen(self.sunfish_data_vendor, timeout=3).read()
            data_json = json.loads(resp.decode('utf-8'))
            if 'datavendor' in data_json.keys():
                l_data_item = data_json['datavendor']
            new_item = 0
            for item in l_data_item:
                vendor = self.env['res.partner'].search([('ref','=',item['Vendor_code'])],limit=1)
                if not vendor:
                    new_vendor = vendor.create({
                                    "name":item['Vendor_name'],
                                    "ref":item['Vendor_code'] 
                                })
                    new_item +=1
                    
            return new_item
        except HTTPError as error:
            _logger.error('Data not retrieved because %s' % error)
            return 0