from odoo import fields, models, api, _
from odoo.exceptions import UserError,Warning
from datetime import datetime
import ast
import json
import logging
_logger = logging.getLogger(__name__)
try:
    import requests
    import urllib.request
except Exception as e:
    _logger.error("Package Not Found !! Because"+ str(e))
    
class PostSunfish(models.TransientModel):
    _name = 'post.sunfish.wizard'

    
    @api.multi
    def post_to_sunfish(self):
        ids_to_change = self._context.get('active_ids')
        doc_ids = self.env['timbangan'].browse(ids_to_change)
        for data in doc_ids:
            po = data.no_po
            ref = data.reference
            item_code = data.product_id.default_code
            no_kendaraan = data.no_pol
            date = datetime.strftime(data.date,"%Y-%m-%d")
            berat_kubik =data.quantity_konversi
            if data.quantity_konversi_compute > 0:
                url_sunfish = "http://192.168.100.17/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=setDataJT&PO_Number="+ po + "&Reference="+ ref +"&Trx_Date="+date+"&Item_Code="+item_code+"&Received_Qty="+"{0:f}".format(data.quantity_konversi_compute)+"&Location=SDG01&Vehicle_Number="+no_kendaraan +"&User_Name=demo"
                try:
                    _logger.warning(url_sunfish)
                    response = urllib.request.urlopen(url_sunfish).read()
                    if self.validateJSON(response):
                        response = json.dumps(response.decode("UTF-8"),default=str)
                        response = json.loads(response)
                        raise Warning("NO PO : " + data.no_po  +"NO REFERENCE " +data.reference +"\n "+"Succesfully POST TO SUNFISH")
                    else:
                        response = response.replace("[", '"')
                        response = response.replace("]",'"')
                        response = ast.literal_eval(response)
                        # response = json.loads(response)
                        _logger.info("="*50)
                        _logger.info("SUNFISH RESPONSE"+str(response))
                        _logger.info(response['return'])
                        _logger.info("="*50)
                        raise Warning("NO PO : " + data.no_po  +"\nNO REFERENCE " +data.reference +"\nFAILED POST TO SUNFISH\nBECAUSE\n"+ str(response["return"]))
                except Exception as e:
                    raise Warning(e)
            else:
                url_sunfish = "http://192.168.100.17/apberp/erp/eaccounting/tools/api/sfservice.cfc?method=setDataJT&PO_Number="+ po + "&Reference="+ ref +"&Trx_Date="+date+"&Item_Code="+item_code+"&Received_Qty="+"{0:f}".format(data.quantity)+"&Location=SDG01&Vehicle_Number="+no_kendaraan +"&User_Name=demo"
                try:
                    response = urllib.request.urlopen(url_sunfish).read()
                    _logger.warning(url_sunfish)
                    if self.validateJSON(response):
                        response = json.dumps(response.decode("UTF-8"),default=str)
                        response = json.loads(response)
                        raise Warning("NO PO : " + data.no_po  +"NO REFERENCE " +data.reference +"\n "+"Succesfully POST TO SUNFISH")
                    else:
                        response = json.dumps(response.decode("UTF-8"),default=str)
                        response = json.loads(response)
                        response = response.replace("[", '"')
                        response = response.replace("]",'"')
                        response = ast.literal_eval(response)
                        # response = json.loads(response)
                        _logger.info("="*50)
                        _logger.info("SUNFISH RESPONSE"+str(response))
                        _logger.info(response['return'])
                        _logger.info("="*50)
                        raise Warning("NO PO : " + data.no_po  +"\nNO REFERENCE " +data.reference  +"\nFAILED POST TO SUNFISH\nBECAUSE\n"+ str(response["return"]))
                        
                except Exception as e:
                    raise Warning(e)
                
                
    def validateJSON(self,data):
        try:
            response = json.dumps(data.decode("UTF-8"),default=str)
            response = json.loads(response)['return']
        except Exception as err:
            return False
        return True