{
    "name": "Accounting Custom",
    "version": "1.0", 
    "depends": [
        "base",
        "account",
        # "stock",
        # "barcodes",
        # "purchase",
        "tj_stock_picking_invoice",
        # "tj_kontrabon",
    ], 
    "author": "angkringan.com",
    "website": "http://www.prointegrated.com",
    "category": "IT support",
    "description": """

Features
======================================================================
* add delay day customer invoice
* add surat jalan vendor bill
""",
"data": [
"views/customer_invoice.xml",
"views/vendor_bill.xml",
],
    "application": False,
    "installable": True,
    "auto_install": False,
}