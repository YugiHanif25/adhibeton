from pychart import arrow
from odoo import models, fields, api, exceptions, _
from datetime import datetime
from odoo import tools

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    invoice_delay = fields.Integer('Delay(Day)')#,compute='_compute_delay'

    # @api.onchange('date_due')
    # @api.depends('date_due')
    # def _compute_delay(self):
    #     for rec in self:
    #         if rec.date_due and rec.state in ('draft','open'):
    #             today = datetime\
    #                 .today().strftime('%Y-%m-%d')
    #             start_date = datetime.strptime(today, tools.DEFAULT_SERVER_DATE_FORMAT)
    #             end_date = datetime.strptime(rec.due_date, tools.DEFAULT_SERVER_DATE_FORMAT)
    #             delta = start_date - end_date
    #             print(delta.days)
                # rec.invoice_late = daysDiff

    @api.multi
    def InvoiceDelay(self):
        cr = self.env.cr
        cr.execute("update account_invoice set invoice_delay = current_date - date_due where state in ('draft','open')")
