from odoo import models, fields, api, exceptions, _

class AccountVendorBill(models.Model):
    _inherit = 'account.invoice'

    no_sj = fields.Char('No Surat Jalan',)